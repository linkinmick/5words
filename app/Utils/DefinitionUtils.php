<?php
/**
 * Created by IntelliJ IDEA.
 * User: mfrausto
 * Date: 8/10/16
 * Time: 5:01 PM
 */

namespace App\Utils;

#include(app_path() . '/simplehtmldom/simple_html_dom.php');

class DefinitionUtils{

    private $DICTIONARY_HOST = "http://www.dictionary.com";

    private $word;
    private $dictionaryWordPath;
    private $dictionaryHtml;
    private $dictionarySectionsData;

    /**
     * DefinitionUtils constructor.
     * @param $word = Word to get all info
     */
    public function __construct($word){
        $this->word = $word;
        $this->dictionaryWordPath = $this->DICTIONARY_HOST.'/browse/'.$this->word; //build the dictionary's word path
        if(checkURL($this->dictionaryWordPath)){
            $this->dictionaryHtml = file_get_html($this->dictionaryWordPath); //Get html from dictionary site
            $this->dictionarySectionsData = $this->dictionaryHtml->find('div.def-list', 0)->find('section'); //Get first div with def-list class
        }
    }



    public function getAllInfo(){
        $arrayData = array();
        if(isset($this->dictionarySectionsData)){
            $arrayData = array('attributes' => count($this->dictionarySectionsData));
            $section = array();
            $sections = array();
            foreach ($this->dictionarySectionsData as $sectionData) {
                $sectionTitle = $sectionData->find('header', 0)->plaintext;//Get section title
                $section['title'] = $sectionTitle;
                $section["definitionData"] = $this->getDefinition($sectionData);
                $sections[] = $section;
            }
            $arrayData['sections'] = $sections;
        }
        return $arrayData;
    }

    public function getDefinition($section){
        $definitions = $section->find('div.def-set');//Get div element with definitions
        $definitionsArray = array();
        foreach ($definitions as $definition){ //For each element inside definitions div
            $definitionElements = $definition->find('div.def-content', 0);//Get all elements (definition and/or example)
            $definitionExample = null;
            $definitionArray = array();
            #var_dump($definitionElements);
            foreach ($definitionElements->children() as $element){ //For each element (definition or example)
                $elementClass = $element->getAttribute('class'); //Get element class
                if(strpos($elementClass, 'example') !== false){ //If element is example
                    $definitionExample = $element->plaintext; //Set example
                    #var_dump($definitionExample);
                }
            }
            $definitionPlain = $definition->plaintext; //Set definition
            if(isset($definitionExample)){ //If definition has example
                $definitionPlain = str_replace($definitionExample, "", $definitionPlain);
                $definitionArray['example'] = $definitionExample;
            }
            $definitionPlain = preg_replace('/(\d+\.)(.+)/i', '$2', $definitionPlain);
            $definitionArray['definition'] = $definitionPlain;
            $definitionsArray[] = $definitionArray;
        }
        return $definitionsArray;
        #var_dump($section);
    }
}