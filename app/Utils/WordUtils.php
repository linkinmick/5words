<?php
namespace App\Utils;
/**
 * Created by IntelliJ IDEA.
 * User: mfrausto
 * Date: 8/10/16
 * Time: 12:46 PM
 */

#include(app_path() . '/simplehtmldom/simple_html_dom.php');

class WordUtils{

    private $AUDIO_HOST = "http://audio.forvo.com:80";
    private $FORVO_HOST = "http://forvo.com";

    private $word;
    private $language;
    private $forvoWordPath;
    private $forvoHtml;
    private $forvoDataArticle;
    private $forvoElements;


    /**
     * WordUtils constructor.
     * @param $word = Word to get all info
     * @param $language = Language to get all word info
     */
    public function __construct($word, $language){
        $this->word = $word;
        $this->language = '#'.$language;
        $this->forvoWordPath = $this->FORVO_HOST.'/word/'.$this->word.'/'.$this->language; //build the forvo's word path
        if(checkURL($this->forvoWordPath)){
            $this->forvoHtml = file_get_html($this->forvoWordPath); //Get html from forvo site
            $this->forvoDataArticle = $this->forvoHtml->find('article.pronunciations', 0); //Get first article with pronunciations class
            $this->forvoElements = $this->forvoDataArticle->children(1)->children(); //Get all list elements (all data)
            #var_dump($elements);
        }
    }

    /**
     * @return mixed
     */
    public function getAllInfo(){
        $arrayData = array();
        if(isset($this->forvoElements)) {
            $arrayData = array('attributes' => count($this->forvoElements));
            $items = array();
            foreach ($this->forvoElements as $element) {
                $item['audiopath'] = $this->getAudioPath($element);
                $item['from'] = $this->getFrom($element);
                $item['uploader'] = $this->getUploader($element);
                $item['votes'] = $this->getVotes($element);
                $items[] = $item;
            }
            $arrayData['audioData'] = $items;
            #var_dump($arrayData);
        }else{
            $arrayData['attributes'] = 0;
            $arrayData['audioData'] = array();
        }
        return $arrayData;
    }

    public function getVotes($element){
        $spanVotes = $element->find('span.num_votes', 0);
        $txtVotes = $spanVotes->plaintext;
        $numVotes = trim(str_replace('votes', '', $txtVotes));
        #var_dump($numVotes);
        return $numVotes;
    }

    public function getUploader($element){
        $elementText = $element->plaintext;
        $pattern = '/Pronunciation by (\s+)(\w+)/';
        preg_match($pattern, $elementText, $matches);
        #var_dump($matches);
        if($matches[2]){
            $name = $matches[2];
            #var_dump($uploader);
        }
        $uploader = (isset($name))?$name:'';
        return $uploader;
    }

    public function getFrom($element){
        $spanFrom = $element->find('span.from', 0);
        #var_dump($spanFrom->innertext);
        return $spanFrom->innertext;
    }

    public function getAudioPath($element){
        $aElement = $element->firstChild();
        $functionStr = $aElement->onclick;
        $pattern = '/(\()(.+)(\))/';
        preg_match($pattern, $functionStr, $matches);
        if($matches[2]){
            $params = explode(",", $matches[2]);
            $mp3Path = base64_decode($params[1]);
            $hostedAudioPath = $this->AUDIO_HOST.'/mp3/'.$mp3Path;
            #var_dump($hostedAudioPath);
        }
        $audioPath = (isset($hostedAudioPath))?$hostedAudioPath:'';
        return $audioPath;
    }
}