<?php
/**
 * Created by PhpStorm.
 * User: mfrausto
 * Date: 2/28/17
 * Time: 4:36 PM
 */

namespace App\Utils;


class BoxRecUtils
{
    private $DICTIONARY_HOST = "http://boxrec.com/";

    private $fiveStarCalPath;
    private $fiveStarCalHtml;
    private $CalendarSectionsData;

    /*private $word;
    private $dictionaryWordPath;
    private $dictionaryHtml;
    private $dictionarySectionsData;*/

    public function __construct(){
        $this->fiveStarCalPath = $this->DICTIONARY_HOST.'/cal_5_star'; //build the dictionary's word path
        if(checkURL($this->fiveStarCalPath)){
            $this->fiveStarCalHtml = file_get_html($this->fiveStarCalPath); //Get html from dictionary site
            $this->CalendarSectionsData = $this->fiveStarCalHtml->find('div[id=page_content]', 0)->find('table.tBoutListTable'); //Get first div with def-list class
        }
    }

    public function getAllInfo(){
        $arrayData = array();
        if(isset($this->fiveStarCalHtml)){
            $one = $this->fiveStarCalHtml->find('div[id=page_content]', 0);
            $two = $one->find('h3[class=pageTitle]');
            echo '<pre>';
            foreach ($two as $h3){
                print_r($h3->plaintext);
                echo '<br>';
            }
        }
        //return $arrayData;
    }

}