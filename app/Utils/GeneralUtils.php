<?php
/**
 * Created by IntelliJ IDEA.
 * User: mfrausto
 * Date: 8/10/16
 * Time: 8:58 PM
 */

namespace App\Utils;

    /*
     * Taken from http://stackoverflow.com/questions/408405/easy-way-to-test-a-url-for-404-in-php
     */
    function checkURL($url){
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        // TODO: expand and improve http error handling
        if($httpCode == 404) {
            return false;
        }

        curl_close($handle);
        return true;
    }
